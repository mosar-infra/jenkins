# ws jenkins / datasources

data "aws_region" "current" {}

data "aws_ecs_cluster" "jenkins_cluster" {
  cluster_name = "jenkins-ecs-cluster-${var.environment}"
}

data "aws_service_discovery_dns_namespace" "namespace" {
  name = "jenkins.local"
  type = "DNS_PRIVATE"
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  dynamic "filter" {
    for_each = local.public_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}
data "aws_lb" "lb" {
  name = "jenkins-lb-${var.environment}"
  tags = {
    Environment = var.environment
    ManagedBy   = "ecs-cluster"
  }
}

data "aws_lb_listener" "lb_listener" {
  load_balancer_arn = data.aws_lb.lb.arn
  port              = 443
}

data "aws_subnet" "public" {
  for_each = toset(data.aws_subnets.public.ids)
  id       = each.value
}

data "aws_security_group" "jenkins_lb" {
  name = "jenkins_lb_sg"
  tags = {
    Environment = var.environment
  }
}

data "aws_vpc" "mosar" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_ecr_repository" "jenkins_master" {
  name = "jenkins-master"
}

data "aws_ecr_repository" "jenkins_build_agent_backend" {
  name = "jenkins-build-agent-backend"
}

data "aws_ecr_repository" "jenkins_build_agent_frontend" {
  name = "jenkins-build-agent-frontend"
}

data "aws_ecr_repository" "mosar_flashcard" {
  name = "mosar-flashcard"
}

data "aws_ecr_repository" "mosar_game" {
  name = "mosar-game"
}

data "aws_ecr_repository" "mosar_frontend" {
  name = "mosar-frontend"
}

data "aws_ecr_repository" "kaniko" {
  name = "kaniko"
}

data "aws_ecr_repository" "jenkins_deploy_agent" {
  name = "jenkins-deploy-agent"
}

