[
{
  "name": "${JENKINS_MASTER_CONTAINER_NAME}",
    "image": "${JENKINS_MASTER_IMAGE}:${JENKINS_MASTER_IMAGE_TAG}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "jenkins-${ENVIRONMENT}-service",
        "awslogs-group": "${LOG_GROUP_NAME}"
      }
    },
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080,
        "protocol": "tcp"
      },
      {
        "containerPort": 50000,
        "hostPort": 50000,
        "protocol": "tcp"
      }

    ],
    "cpu": 1,
    "environment": [
      {
        "name": "ENVIRONMENT",
        "value": "${ENVIRONMENT}"
      },
      {
        "name": "JENKINS_USER",
        "value": "${JENKINS_USER1}"
      },
      {
        "name": "JENKINS_PASSWORD",
        "value": "${JENKINS_PASSWORD1}"
      },
      {
        "name": "JENKINS_URL",
        "value": "${JENKINS_URL1}"
      },
      {
        "name": "GITLAB_USER",
        "value": "${GITLAB_USER1}"
      },
      {
        "name": "GITLAB_PASSWORD",
        "value": "${GITLAB_PASSWORD1}"
      },
      {
        "name": "GITLAB_API_TOKEN",
        "value": "${GITLAB_API_TOKEN1}"
      },
      {
        "name": "LOG_GROUP_NAME",
        "value": "${LOG_GROUP_NAME}"
      },
      {
        "name": "LOCK_TABLE_GAME_PROD",
        "value": "${LOCK_TABLE_GAME_PROD}"
      },
      {
        "name": "LOCK_TABLE_FRONTEND_PROD",
        "value": "${LOCK_TABLE_FRONTEND_PROD}"
      },
      {
        "name": "LOCK_TABLE_FLASHCARD_PROD",
        "value": "${LOCK_TABLE_FLASHCARD_PROD}"
      },
      {
        "name": "LOCK_TABLE_GAME_TEST",
        "value": "${LOCK_TABLE_GAME_TEST}"
      },
      {
        "name": "LOCK_TABLE_FRONTEND_TEST",
        "value": "${LOCK_TABLE_FRONTEND_TEST}"
      },
      {
        "name": "LOCK_TABLE_FLASHCARD_TEST",
        "value": "${LOCK_TABLE_FLASHCARD_TEST}"
      },
      {
        "name": "ECS_AGENT_CLUSTER",
        "value": "${ECS_AGENT_CLUSTER}"
      },
      {
        "name": "AWS_REGION",
        "value": "${AWS_REGION}"
      },
      {
        "name": "PRIVATE_JENKINS_HOST_AND_PORT",
        "value": "${PRIVATE_JENKINS_HOST_AND_PORT}"
      },
      {
        "name": "AGENT_TASK_ROLE_ARN",
        "value": "${AGENT_TASK_ROLE_ARN}"
      },
      {
        "name": "AGENT_SECURITY_GROUP_ID",
        "value": "${AGENT_SECURITY_GROUP_ID}"
      },
      {
        "name": "AGENT_EXECUTION_ROLE_ARN",
        "value": "${AGENT_EXECUTION_ROLE_ARN}"
      },
      {
        "name": "KANIKO_IMAGE",
        "value": "${KANIKO_IMAGE}"
      },
      {
        "name": "KANIKO_FRONTEND_FAMILY",
        "value": "${KANIKO_FRONTEND_FAMILY}"
      },
      {
        "name": "KANIKO_FLASHCARD_FAMILY",
        "value": "${KANIKO_FLASHCARD_FAMILY}"
      },
      {
        "name": "KANIKO_GAME_FAMILY",
        "value": "${KANIKO_GAME_FAMILY}"
      },
      {
        "name": "JENKINS_BUILD_AGENT_FRONTEND_IMAGE",
        "value": "${JENKINS_BUILD_AGENT_FRONTEND_IMAGE}"
      },
      {
        "name": "JENKINS_BUILD_AGENT_BACKEND_IMAGE",
        "value": "${JENKINS_BUILD_AGENT_BACKEND_IMAGE}"
      },
      {
        "name": "JENKINS_DEPLOY_AGENT_TASK_ROLE_ARN",
        "value": "${JENKINS_DEPLOY_AGENT_TASK_ROLE_ARN}"
      },
      {
        "name": "JENKINS_DEPLOY_AGENT_IMAGE",
        "value": "${JENKINS_DEPLOY_AGENT_IMAGE}"
      },
      {
        "name": "BUCKET_FRONTEND",
        "value": "${BUCKET_FRONTEND}"
      },
      {
        "name": "BUCKET_FLASHCARD",
        "value": "${BUCKET_FLASHCARD}"
      },
      {
        "name": "BUCKET_GAME",
        "value": "${BUCKET_GAME}"
      },
      {
        "name": "MOSAR_FLASHCARD_REPO",
        "value": "${MOSAR_FLASHCARD_REPO}"
      },
      {
        "name": "MOSAR_FRONTEND_REPO",
        "value": "${MOSAR_FRONTEND_REPO}"
      },
      {
        "name": "MOSAR_GAME_REPO",
        "value": "${MOSAR_GAME_REPO}"
      },
      {
        "name": "SPRING_PROFILES_ACTIVE",
        "value": "${SPRING_PROFILES_ACTIVE}"
      },
      {
        "name": "SUBNET_IDS",
        "value": "${SUBNET_IDS}"
      }
    ],
    "secrets": [],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 2048,
    "volumesFrom": []
  }
]
