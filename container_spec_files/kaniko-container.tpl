[
{
  "name": "kaniko",
    "image": "${KANIKO_IMAGE}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "jenkins-kaniko-${ENVIRONMENT}",
        "awslogs-group": "${LOG_GROUP_NAME}"
      }
    },
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080,
        "protocol": "tcp"
      },
      {
        "containerPort": 50000,
        "hostPort": 50000,
        "protocol": "tcp"
      }

    ],
    "cpu": 1,
    "environment": [],
    "secrets": [],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 1024,
    "volumesFrom": []
  }
]
