# ws jenkins / outputs

output "ecs-app" {
  value     = module.ecs-app
  sensitive = true
}

output "iam" {
  value     = module.iam
  sensitive = true
}

output "security_groups" {
  value = module.security_groups
}

output "secrets" {
  value     = module.secrets
  sensitive = true
}

output "s3" {
  value = module.s3
}

output "lock_table" {
  value = aws_dynamodb_table.dynamodb-terraform-state-lock
}

output "kaniko_task" {
  value = aws_ecs_task_definition.kaniko_task
}
