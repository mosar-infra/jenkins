# ws jenkins/locals.tf

locals {
  dns_ip = "12.12.0.2"
}

locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  jenkins_names = {
    name           = "jenkins"
    container_name = "jenkins-${var.environment}"
    namespace_id   = data.aws_service_discovery_dns_namespace.namespace.id
    namespace_name = data.aws_service_discovery_dns_namespace.namespace.name
  }
}

locals {
  roles = {
    jenkins_task = {
      role = {
        name               = "jenkins-ecs-task-role"
        description        = "jenkins role tasks"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "jenkins-ecs-task-policy"
        description = "Policy for jenkins task role"
        policy      = file("${path.root}/iam_spec_files/policy_ecs_task.json")
      }
      policy_attachment = {
        name = "jenkins-ecs-task-policy-attachment"
      }
    }
    kaniko_task = {
      role = {
        name               = "kaniko-ecs-task-role"
        description        = "kaniko role tasks"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "kaniko-ecs-task-policy"
        description = "Policy for kaniko task role"
        policy      = file("${path.root}/iam_spec_files/kaniko_task_policy.json")
      }
      policy_attachment = {
        name = "kaniko-ecs-task-policy-attachment"
      }
    }
    jenkins_deploy_task = {
      role = {
        name               = "jenkins-deploy-ecs-task-role"
        description        = "deploy role tasks"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "jenkins-deploy-ecs-task-policy"
        description = "Policy for deployments to ecs task role"
        policy      = file("${path.root}/iam_spec_files/jenkins_deploy_task_policy.json")
      }
      policy_attachment = {
        name = "jenkins-deploy--ecs-task-policy-attachment"
      }
    }
    jenkins_task_execution = {
      role = {
        name               = "jenkins-ecs-task-execution-role"
        description        = "role to execute the jenkins ecs tasks"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "jenkins-ecs-task-execution-policy"
        description = "Policy to execute ecs jenkins tasks"
        policy      = file("${path.root}/iam_spec_files/policy_ecs_task_execution.json")
      }
      policy_attachment = {
        name = "jenkins-ecs-task-execution-policy-attachment"
      }
    }
  }
}

locals {
  apps = {
    jenkins = {
      name                  = "jenkins"
      desired_count         = 1
      container_definitions = templatefile("${path.root}/container_spec_files/jenkins-master-container.tpl", local.container_vars)
      subnet_ids            = local.subnets.private
      security_groups = concat(
        module.security_groups.separate_security_groups["jenkins_master"].*.id,
        module.security_groups.security_groups["jenkins_dns"].*.id
      )
      assign_public_ip        = false
      family                  = "jenkins-family"
      use_load_balancer       = true
      lb_target_group_arn     = data.aws_lb_listener.lb_listener.default_action[0].target_group_arn
      lb_container_name       = local.jenkins_names.container_name
      lb_container_port       = 8080
      ecs_task_execution_role = module.iam.role["jenkins_task_execution"]
      ecs_task_role           = module.iam.role["jenkins_task"]
      cpu                     = 1024
      memory                  = 2048
      cluster                 = data.aws_ecs_cluster.jenkins_cluster
      discovery_service = {
        name              = local.jenkins_names.name
        namespace_id      = local.jenkins_names.namespace_id
        ttl               = 30
        failure_threshold = 5
      }
      autoscaling = {
        max_capacity = 3
        min_capacity = 1
      }
      autoscaling_policies = {
        memory = {
          name                   = "jenkins_memory_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageMemoryUtilization"
          target_value           = 80
        }
        cpu = {
          name                   = "jenkins_cpu_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageCPUUtilization"
          target_value           = 60
        }
      }
    }
  }
}
locals {
  security_groups = {
    jenkins_slaves = {
      name        = "jenkins_internal_sg"
      vpc_id      = data.aws_vpc.mosar.id
      description = "security group for internal jenkins slaves"
      ingress_sg  = {}
      ingress_cidr = {
        http = {
          from        = 50000
          to          = 50000
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
    jenkins_dns = {
      name        = "dns_sg"
      vpc_id      = data.aws_vpc.mosar.id
      description = "security group for dns routing traffic"
      ingress_sg  = {}
      ingress_cidr = {
        tcp = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}


locals {
  separate_security_groups = {
    jenkins_master = {
      name         = "jenkins_private_sg"
      vpc_id       = data.aws_vpc.mosar.id
      description  = "security group for jenkins master"
      ingress_cidr = {}
      ingress_sg = {
        http = {
          from            = 8080
          to              = 8080
          protocol        = "tcp"
          security_groups = [data.aws_security_group.jenkins_lb.id]
        }
        jnlp = {
          from            = 50000
          to              = 50000
          protocol        = "tcp"
          security_groups = [data.aws_security_group.jenkins_lb.id]
        }
      }
      egress = {
        http = {
          from        = 8080
          to          = 8080
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
        jnlp = {
          from        = 50000
          to          = 50000
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  secrets = { for k, v in var.predefined_secrets : k => module.secrets.predefined_secret_versions[k].secret_string }
}

locals {
  container_config_vars = {
    AGENT_EXECUTION_ROLE_ARN           = module.iam.role["jenkins_task_execution"].arn
    AGENT_SECURITY_GROUP_ID            = module.security_groups.security_groups["jenkins_slaves"].id
    AGENT_TASK_ROLE_ARN                = module.iam.role["jenkins_task"].arn
    AWS_REGION                         = data.aws_region.current.name
    BUCKET_FLASHCARD                   = module.s3["build_mosar_flashcard"].bucket.bucket
    BUCKET_FRONTEND                    = module.s3["build_mosar_frontend"].bucket.bucket
    BUCKET_GAME                        = module.s3["build_mosar_game"].bucket.bucket
    LOCK_TABLE_GAME_PROD               = aws_dynamodb_table.dynamodb-terraform-state-lock["game_prod"].name
    LOCK_TABLE_FLASHCARD_PROD          = aws_dynamodb_table.dynamodb-terraform-state-lock["flashcard_prod"].name
    LOCK_TABLE_FRONTEND_PROD           = aws_dynamodb_table.dynamodb-terraform-state-lock["frontend_prod"].name
    LOCK_TABLE_GAME_TEST               = aws_dynamodb_table.dynamodb-terraform-state-lock["game_test"].name
    LOCK_TABLE_FLASHCARD_TEST          = aws_dynamodb_table.dynamodb-terraform-state-lock["flashcard_test"].name
    LOCK_TABLE_FRONTEND_TEST           = aws_dynamodb_table.dynamodb-terraform-state-lock["frontend_test"].name
    JENKINS_MASTER_CONTAINER_NAME      = local.jenkins_names.container_name
    JENKINS_DEPLOY_AGENT_TASK_ROLE_ARN = module.iam.role["jenkins_deploy_task"].arn
    JENKINS_DEPLOY_AGENT_IMAGE         = "${data.aws_ecr_repository.jenkins_deploy_agent.repository_url}:${var.jenkins_deploy_agent_image_tag}"
    ECS_AGENT_CLUSTER                  = data.aws_ecs_cluster.jenkins_cluster.cluster_name
    ENVIRONMENT                        = var.environment
    JENKINS_BUILD_AGENT_BACKEND_IMAGE  = "${data.aws_ecr_repository.jenkins_build_agent_backend.repository_url}:${var.jenkins_build_agent_backend_image_tag}"
    JENKINS_BUILD_AGENT_FRONTEND_IMAGE = "${data.aws_ecr_repository.jenkins_build_agent_frontend.repository_url}:${var.jenkins_build_agent_frontend_image_tag}"
    JENKINS_MASTER_IMAGE               = data.aws_ecr_repository.jenkins_master.repository_url
    JENKINS_MASTER_IMAGE_TAG           = var.jenkins_master_image_tag
    KANIKO_FLASHCARD_FAMILY            = aws_ecs_task_definition.kaniko_task["flashcard"].family
    KANIKO_FRONTEND_FAMILY             = aws_ecs_task_definition.kaniko_task["frontend"].family
    KANIKO_GAME_FAMILY                 = aws_ecs_task_definition.kaniko_task["game"].family
    KANIKO_IMAGE                       = "${data.aws_ecr_repository.kaniko.repository_url}:${var.kaniko_image_tag}"
    LOG_GROUP_NAME                     = "jenkins-logs-${var.environment}"
    MOSAR_FLASHCARD_REPO               = "${data.aws_ecr_repository.mosar_flashcard.repository_url}"
    MOSAR_FRONTEND_REPO                = "${data.aws_ecr_repository.mosar_frontend.repository_url}"
    MOSAR_GAME_REPO                    = "${data.aws_ecr_repository.mosar_game.repository_url}"
    PRIVATE_JENKINS_HOST_AND_PORT      = "${local.jenkins_names.name}.${local.jenkins_names.namespace_name}:50000"
    SPRING_PROFILES_ACTIVE             = var.spring_profiles_active
    SUBNET_IDS                         = join(",", local.subnets.private)
  }
}

locals {
  kaniko_container_vars = {
    ENVIRONMENT    = var.environment
    LOG_GROUP_NAME = "jenkins-logs-${var.environment}"
    KANIKO_IMAGE   = "${data.aws_ecr_repository.kaniko.repository_url}:${var.kaniko_image_tag}"
  }
}
locals {
  kaniko_tasks = {
    flashcard = {
      family                = "kaniko-flashcard-family"
      execution_role_arn    = module.iam.role["jenkins_task_execution"].arn
      task_role_arn         = module.iam.role["kaniko_task"].arn
      cpu                   = 512
      memory                = 1024
      container_definitions = templatefile("${path.root}/container_spec_files/kaniko-container.tpl", local.kaniko_container_vars)
    }
    game = {
      family                = "kaniko-game-family"
      execution_role_arn    = module.iam.role["jenkins_task_execution"].arn
      task_role_arn         = module.iam.role["kaniko_task"].arn
      cpu                   = 512
      memory                = 1024
      container_definitions = templatefile("${path.root}/container_spec_files/kaniko-container.tpl", local.kaniko_container_vars)
    }
    frontend = {
      family                = "kaniko-frontend-family"
      execution_role_arn    = module.iam.role["jenkins_task_execution"].arn
      task_role_arn         = module.iam.role["kaniko_task"].arn
      cpu                   = 512
      memory                = 1024
      container_definitions = templatefile("${path.root}/container_spec_files/kaniko-container.tpl", local.kaniko_container_vars)
    }
  }
}

locals {
  buckets = {
    build_mosar_flashcard = {
      version        = "version_1" # just for keepers
      bucket         = "build-mosar-flashcard"
      acl            = "private"
      file_directory = ""
    }
    build_mosar_game = {
      version        = "version_1" # just for keepers
      bucket         = "build-mosar-game"
      acl            = "private"
      file_directory = ""
    }
    build_mosar_frontend = {
      version        = "version_1" # just for keepers
      bucket         = "build-mosar-frontend"
      acl            = "private"
      file_directory = ""
    }
    terraform-state-test = {
      version        = "test_1" # just for keepers
      bucket         = "terraform-state-test"
      acl            = "private"
      file_directory = ""
    }
    terraform-state-prod = {
      version        = "prod_1" # just for keepers
      bucket         = "terraform-state-prod"
      acl            = "private"
      file_directory = ""
    }
  }
}


locals {
  container_vars = merge(local.secrets, local.container_config_vars)
}

locals {
  lock_tables = {
    flashcard_test = {
      name = "flashcard_test"
    }
    game_test = {
      name = "game_test"
    }
    frontend_test = {
      name = "frontend_test"
    }
    flashcard_prod = {
      name = "flashcard_prod"
    }
    game_prod = {
      name = "game_prod"
    }
    frontend_prod = {
      name = "frontend_prod"
    }
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}

locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}

locals {
  lb_listener_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:ManagedBy"
      values = ["ecs-cluster"]
  }]
}



