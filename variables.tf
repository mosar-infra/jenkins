# root/variables.tf

variable "jenkins_master_image_tag" {}
variable "environment" {}
variable "managed_by" {}
variable "predefined_secrets" {}
variable "kaniko_image_tag" {}
variable "jenkins_build_agent_frontend_image_tag" {}
variable "jenkins_build_agent_backend_image_tag" {}
variable "jenkins_deploy_agent_image_tag" {}
variable "spring_profiles_active" {}
