# ws jenkins /main.tf

module "ecs-app" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-ecs-app.git?ref=tags/v1.0.6"
  for_each    = local.apps
  app         = each.value
  environment = var.environment
  managed_by  = var.managed_by
}

module "security_groups" {
  source                   = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment              = var.environment
  managed_by               = var.managed_by
  security_groups          = local.security_groups
  separate_security_groups = local.separate_security_groups
}

module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.1.2"
  roles       = local.roles
  environment = var.environment
  managed_by  = var.managed_by
}

module "secrets" {
  source             = "git::https://gitlab.com/mosar-infra/tf-module-secrets.git?ref=tags/v3.2.0"
  predefined_secrets = var.predefined_secrets
  environment        = var.environment
  managed_by         = var.managed_by
}

module "s3" {
  source         = "git::https://gitlab.com/mosar-infra/tf-module-s3.git?ref=tags/v1.0.9"
  for_each       = local.buckets
  bucket         = "${each.value.bucket}-${random_string.string.result}"
  acl            = each.value.acl
  file_directory = each.value.file_directory
  environment    = var.environment
  managed_by     = var.managed_by
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  for_each       = local.lock_tables
  name           = "${each.value.name}-tf-state-lock-dynamo"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
    Name        = "DynamoDB Terraform State Lock Table for ${each.value.name}"
  }
}

resource "random_string" "string" {
  length  = 4
  special = false
  upper   = false
  keepers = {
    name = local.buckets["build_mosar_game"].version
  }
}

resource "aws_ecs_task_definition" "kaniko_task" {
  for_each                 = local.kaniko_tasks
  family                   = each.value.family
  network_mode             = "awsvpc"
  execution_role_arn       = each.value.execution_role_arn
  task_role_arn            = each.value.task_role_arn
  cpu                      = each.value.cpu
  memory                   = each.value.memory
  requires_compatibilities = ["FARGATE"]
  container_definitions    = each.value.container_definitions

  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

